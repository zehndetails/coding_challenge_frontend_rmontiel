# Front End Coding Challenge

## What is this repository for?
This repository should be used to push your solution for the Morressier front-end coding challenge. 

## How do I get set up?
We have build a small backend to which you should build a frontend. The documentation of the API can be found [here](http://mcc17.herokuapp.com/)
Please commit your code constantly into the repository. Just work as you would normally do it and once you’ve completed the task, just let us know. We will be reviewing your solution and will invite you for a personal meeting in our office. 

## Background information
We are transforming the academic industry into digital by converting traditional paper posters into e-posters. These e-posters will be presented on major international conferences with our iPad App (connected to a screen or projector). In order to manage these eposters, we’ve crafted www.morressier.com. 

## Task
You will help the a team of conference organisers and provide them with a frontend for their 'organisers backoffice'. The user of the front end should be able to do the following:
* Create organiser account
* Create Posters and authors
* Rename Posters (change ID, Title)
* Change email address of authors
* Delete Poster

## Please note
* We implemented a REST api with support for HTTP HEAD, OPTIONS, GET POST and DELETE methods
* HTTP PUT is not supported
* Do not focus on the visual screen design, just concentrate on the functionality. 
* Do it simple and focus on the pipeline (dev > test > deploy)

# Screendesign description
## General use of InvisionApp Mockup
https://invis.io/YTCU73DXG leads you to the screen design mockup. The mockup contains “hotspot” which are clickable and lead you through the mockup. Holding shift on a page highlights the clickable area.

## Screen 1 – Sign Up Empty
https://invis.io/YTCU73DXG#/246482700_1_-_Sign_Up_Emtpy

This screen just lets the user create an organiser. We currently do NOT require a password. What happens if user already exists? You decide.

## Screen 2 – Sign Up Filled
https://invis.io/YTCU73DXG#/246482701_2_-_Sign_Up_Filled

Same as Screen 1, but with filled text fields

## Screen 3 - Manage posters (Adding Posters + Authors)
https://invis.io/YTCU73DXG#/246482702_3_-_Manage_Posters

After having created the organiser, the user will be landing on his poster-overview. Here the user is able to create posters together with authors. The user will be creating a poster and author (which don’t exist separately) by clicking “Create Author + Poster”. 

## Screen 4 – Typing poster details (ID, Email Address, Poster-title)
https://invis.io/YTCU73DXG#/246482703_4_-_Create_Poster___Author

The list of posters will be appearing where the user can enter Poster ID, email address of the author and title of the poster. 

## Screen 5 - Managing Posters
https://invis.io/YTCU73DXG#/246482705_5_-_Manage_Posters

Just clicking into the text the user can change the attributes of the Poster (email of author, ID, title ). 

## Screen 6 – Deleting Posters
https://invis.io/YTCU73DXG#/263871725_6_-_Delete_Poster

Hovering over a poster from the list will show the 'delete' button which the user can use to delete a poster.

# What we expect
* test driven development if possible
* ReactJS
* pipeline: dev > test > deploy
* integrate build-tool of your choice
* provide information on how to build and test your application 

## Who do I talk to if something is unclear?
* Do not hesitate to contact justus.weweler@morressier.com for general questions regarding the challenge and the related tasks. 
* For technical questions, reach out to martin.kluth@morressier.com

## API Documentation
The API Docu contains more information than you will actually need. Just use what you need.  